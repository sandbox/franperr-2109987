This module changes massively the author of nodes, from one user to 
another, by node types, using a batch process.

It provides a link in administration menu (/admin/config/content/authorship) 
and also shows a message when a user is blocked if this user is the author of
at least one node.

It is useful if in your drupal setup, the node author is the only one in charge
of managing its own nodes (for example answering comments...) and leave the 
organization, so you can assign its nodes to another user that will in charge of
"managing" them.
